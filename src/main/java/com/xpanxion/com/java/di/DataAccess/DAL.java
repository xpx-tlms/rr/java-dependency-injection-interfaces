package com.xpanxion.com.java.di.DataAccess;

import com.xpanxion.com.java.di.Enums.Environment;
import com.xpanxion.com.java.di.Models.PersonRecord;
import com.xpanxion.com.java.di.Proxies.DBProxy;
import com.xpanxion.com.java.di.Proxies.MockDBProxy;

public class DAL { // Essentially a factory that create a DataAccess object.

    //
    // Data Members
    //

    private final DataAccess dataAccess;

    //
    // Constructors
    //

    public DAL(Environment env ) {
        if (env.equals(Environment.PROD)) {
            this.dataAccess = new DataAccess(new DBProxy()); // Injecting something that implements the DataAccessable interface.
        } else {
            this.dataAccess = new DataAccess(new MockDBProxy()); // Injecting something that implements the DataAccessable interface.
        }
    }

    //
    // Accessors
    //

    //
    // Public Methods
    //

    public PersonRecord getPerson(int personId) {
        return this.dataAccess.getPerson(personId);
    }

    //
    // Overrides
    //

    //
    // Private Methods
    //
}
