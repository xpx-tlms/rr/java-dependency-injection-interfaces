package com.xpanxion.com.java.di.DataAccess;

import com.xpanxion.com.java.di.Interfaces.DataAccessable;
import com.xpanxion.com.java.di.Models.PersonRecord;

public class DataAccess {

    //
    // Data Members
    //

    private DataAccessable proxy;

    //
    // Constructors
    //

    public DataAccess(DataAccessable aProxy) {
        this.proxy = aProxy;
    }

    //
    // Accessors
    //

    //
    // Public Methods
    //

    public PersonRecord getPerson(int personId) {
        return this.proxy.getPerson(personId);
    }

    //
    // Overrides
    //

    //
    // Private Methods
    //
}
