package com.xpanxion.com.java.di.Interfaces;

import com.xpanxion.com.java.di.Models.PersonRecord;

public interface DataAccessable {

    PersonRecord getPerson(int personId);

}
