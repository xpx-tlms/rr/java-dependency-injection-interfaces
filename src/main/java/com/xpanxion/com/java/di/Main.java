package com.xpanxion.com.java.di;

import com.xpanxion.com.java.di.Enums.Environment;
import com.xpanxion.com.java.di.DataAccess.DAL;

public class Main {
    public static void main(String[] args) {
        var dal = new DAL(Environment.DEV);
        var pr = dal.getPerson(1);
        System.out.println(pr.name());
    }
}
