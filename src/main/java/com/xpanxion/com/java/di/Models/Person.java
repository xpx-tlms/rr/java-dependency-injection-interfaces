package com.xpanxion.com.java.di.Models;

public class Person {

    //
    // Data Members
    //

    private String name;

    //
    // Constructors
    //

    public Person(String name) {
        this.name = name;
    }

    //
    // Accessors
    //

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //
    // Public Methods
    //

    public String speak() {
        return String.format("Hello, my name is %s", this.name);
    }

    //
    // Overrides
    //

    //
    // Private Methods
    //
}
