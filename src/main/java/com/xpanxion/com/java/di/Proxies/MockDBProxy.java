package com.xpanxion.com.java.di.Proxies;

import com.xpanxion.com.java.di.Interfaces.DataAccessable;
import com.xpanxion.com.java.di.Models.PersonRecord;

public class MockDBProxy implements DataAccessable {

    //
    // Data Members
    //

    //
    // Constructors
    //

    //
    // Accessors
    //

    //
    // Public Methods
    //

    //
    // Overrides
    //

    @Override
    public PersonRecord getPerson(int personId) {
        return new PersonRecord("Fake Fred");
    }

    //
    // Private Methods
    //

}
